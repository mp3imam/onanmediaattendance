<!DOCTYPE html>
<html>

<head>
    <title>laravel webcam capture image and save from camera - ItSolutionStuff.com</title>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.25/webcam.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <style type="text/css">
        #results {
            padding: 20px;
            border: 1px solid;
            background: #ccc;
        }
    </style>
</head>

<body>

    <div class="container">
        <h1 class="text-center">Onanmedia Attendance</h1>
        <form id="capture-form" method="POST" action="{{ route('webcam.capture') }}">
            @csrf
            <div class="row">
                <div class="col-md-6">
                    <div id="my_camera"></div>
                    <br />
                    <input type="hidden" name="image" class="image-tag">
                </div>
                <div class="col-md-6 text-center">
                    <div class="row fw-bold">
                        <div class="col-md-1">No</div>
                        <div class="col-md-4">Foto</div>
                        <div class="col-md-3">Status</div>
                        <div class="col-md-4">Waktu</div>
                    </div>
                    <div class="row" style="height: 300px; overflow-y: scroll;">
                        @foreach ($data_absen as $data)
                            <div class="col-md-1 py-4">{{ $loop->iteration }}</div>
                            <div class="col-md-4 py-2 text-center"><img src="{{ $data->foto }}" width="170px"
                                    height="80px">
                            </div>
                            <div class="col-md-3 py-4">
                                @if (Carbon\Carbon::parse($data->created_at)->format('H:i:s a') > '08:20:00 am')
                                    <span class="badge bg-warning">Telat Hadir</span>
                                @else
                                    <span class="badge bg-success">Hadir</span>
                                @endif
                            </div>
                            <div class="col-md-4 py-4">
                                {{ Carbon\Carbon::parse($data->created_at)->format('H:i:s a') }}
                            </div>
                        @endforeach
                    </div>
                    <div class="mt-4">
                        Total Hadir : <strong>{{ $data_absen->count() }}</strong> orang
                    </div>
                </div>
                <div class="col-md-12 text-center">
                    <br />
                    <button type="button" class="btn btn-success" onClick="take_snapshot()">Submit</button>
                </div>
                <div class="col-md-6" hidden>
                    <div id="results">Your captured image will appear here...</div>
                </div>
            </div>
        </form>
    </div>

    <script language="JavaScript">
        Webcam.set({
            width: 490,
            height: 350,
            image_format: 'jpeg',
            jpeg_quality: 90
        });

        Webcam.attach('#my_camera');

        function take_snapshot() {
            Webcam.snap(function(data_uri) {
                $(".image-tag").val(data_uri);
                document.getElementById('results').innerHTML = '<img src="' + data_uri + '"/>';
                document.getElementById('capture-form').submit();
            });
        }
    </script>

</body>

</html>
