<?php

use App\Http\Controllers\WebcamController;
use Illuminate\Support\Facades\Route;

Route::get('/', [WebcamController::class, 'index']);
Route::post('webcam', [WebcamController::class, 'store'])->name('webcam.capture');